# Ladue CS Challenge 2.0 - Data Overload!

All the problems in this round were prepared by Anthony. I tried to make the problems generally easy especially since this set was first tested by middle schoolers, but they tend to be a bit more difficult than I expect. I did kind of get lazy by making hard versions instead of completely new problems, but the bonus problems, especially the last one, are quite interesting to think about. You can find all the problems and hints in this repository.


## A

This problem relies on the well-known trick of right-clicking a web page and clicking "Inspect" to enter the element inspector. In this mode, you can see the source code for the website. Doing this trick on exozy.me reveals the HTML comment "I has no HTML skillz", the answer.


## B

Do you know how to scan a QR code? If you don't, search engines are your friend!


## C

Not too hard if you interpret the problem literally. The answer is right in your face!


## D

Same as A, but with a programming component. The GCD program is not too difficult to write; even something with three nested loops, two to iterate over the ordered pairs, and one to check all numbers if it's the GCD, will work.


## E

This is such a fun problem! If you scroll down on one of the pages, the answer will be clear! Or inspect element, either works!


## F

"Explore" is a big hint: click "explore" on Gitea and the solution is right down below.


## G

This one is a bit tricky, but a [fast GCD algorithm](https://en.wikipedia.org/wiki/Euclidean_algorithm) should allow this to run in a relatively reasonable amount of time. Even something in Python will work in under 10 minutes.


## H

Brute force. I hate you.


## I

The Gitea password is the [smallest 6 digit prime number](https://www.wolframalpha.com/input/?i=smallest+6+digit+prime+number). Log in to access a secret repository which contains another problem! The problem is quite the brain teaser, but something like [this wonderful site](https://onelook.com/) should allow you to quickly find the answer... if you know regular expressions!


## J

All the clues are in the hints from the previous problems. Go to the [website from the hint in G](ta180m.exozy.me) and log in as the guest user with password "LadueComputerScience". Now click on the tab that says "Virtual Machines" and you're almost there. You can try all the VMs, but J's hint gives the direct answer: the Ubuntu VM has the flag! Just turn it on and you should find it sitting right on the desktop.


## Bonus 1

You need a fast algorithm for this one. One way is to count for each number, how many ordered pairs have this number as their GCD. You can use DP to implement this in approximately `O(N log(N) sqrt(N))`. (Email me for more details if you are interested)


## Bonus 2

Pretty easy, just click shut down on the front page of [the website from J](ta180m.exozy.me). Control panels are awesome!


## Bonus 3

Same as Bonus 1, but you have to take out the `log(N)` from the time complexity in your implementation for it to run in a reasonable time. This is quite advanced, but if you want more details, just email me.

