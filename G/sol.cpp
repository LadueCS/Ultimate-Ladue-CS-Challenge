#include <bits/stdc++.h>
using namespace std;
using ll = long long;

int main() {
    int N = 1e5;
    ll ans = 0;
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j) ans += __gcd(i, j);
    cout << ans;
}
